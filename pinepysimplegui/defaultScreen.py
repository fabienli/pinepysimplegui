import PySimpleGUI as sg

def getTopMenu():
    return [sg.Button('Main Screen'), sg.Button('Infos'), sg.Button('Close')]

def handleMenuEvents(window):
    # Event Loop to process "events"
    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == 'Close': # if user closes window or clicks an exit button
            window.close()
            break
        elif event == 'Infos': # if user closes window or clicks an exit button
            window.close()
            showInfoScreen()
            break
        elif event == 'Main Screen': # if user closes window or clicks an exit button
            window.close()
            showDemoScreen()
            break

def showDefaultScreen(realContent):
    sg.theme('LightBlue')
    myColumn = sg.Column(realContent,  scrollable=True, vertical_scroll_only=True, expand_x=True, expand_y=True)
    layout = [getTopMenu(), [myColumn]]
    window = sg.Window("test scroll", layout, margins=(2, 2), auto_size_text=True, resizable=True).Finalize()
    myColumn.expand(True,True,True)
    handleMenuEvents(window)

def showInfoScreen():
    infos=[ [sg.Text('PinePySimpleGUI')],
            [sg.Text('This is a sample of using pySimpleGUI, mainly targeted to run on pinephone.')],
            [sg.Text('Find source code at https://gitlab.com/fabienli/pinepysimplegui')] ]
    showDefaultScreen(infos)

def showDemoScreen():
    pageContent = []
    for i in range(1,10):
        pageContent+= [ [sg.Text('Lorem Ipsum %2d'%i, key='-title%d-'%i)],
                        [sg.Text('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', key='-comment%d-'%i)] ]
    showDefaultScreen(pageContent)
