rm -rf release
mkdir -p release/pinepysimplegui
mkdir release/pinepysimplegui/DEBIAN
mkdir -p release/pinepysimplegui/usr/bin
echo "Package: pinepysimplegui
Version: 0.01
Architecture: all
Maintainer: Fabien<fab@lisiecki.fr>
Build-Depends: debhelper (>= 9), python, python3-tk, dh-virtualenv (>= 0.7)
Installed-Size: 2
Pre-Depends: dpkg (>= 1.16.1), python3-tk
Depends: python3-tk
Section: extras
Priority: optional
Homepage: https://gitlab.com/fabienli/pinepysimplegui
Description: demo of pySimpleGUI on pinephone" > release/pinepysimplegui/DEBIAN/control
cp -r delivery/pinePySimpleGUI release/pinepysimplegui/usr/bin/
mkdir release/pinepysimplegui/usr/bin/pinepysimplegui
cp -r pinepysimplegui/*.py release/pinepysimplegui/usr/bin/pinepysimplegui
cd release
dpkg -b pinepysimplegui
rm -rf pinepysimplegui
cd ..
