# pinePySimpleGUI
this is a sample of using pySimpleGUI, mainly targeted to run on pinephone.


# simple launch script:
`./run_local.sh`

or you can also go through the manual setup:

# manual install
`python3 -m venv env
source env/bin/activate
pip3 install pysimplegui`

# run
`python pinepysimplegui`

# package install
This was only tested on pinephone's mobian:
1. Download the deb package from:  https://gitlab.com/fabienli/pinepysimplegui/-/raw/master/release/pinepysimplegui.deb ̀.   
2. install it via `sudo dpkg -i  pinepysimplegui.deb`
3. launch it with the command `pinePySimpleGUI`
