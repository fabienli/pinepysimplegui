#!/bin/bash
VENV="./env"
MAIN="pinepysimplegui"
if [ ! -d $VENV ]
then
  python3 -m venv $VENV
  source $VENV/bin/activate
  pip3 install pysimplegui
  deactivate
fi
source $VENV/bin/activate
python $MAIN
deactivate
